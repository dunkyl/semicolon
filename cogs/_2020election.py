from datetime import datetime, timedelta

import humanize
from discord.ext import tasks, commands

from .utils.bot import Semicolon
from .utils.constants import CONSTANTS
from .utils.errors import WebException
from .utils.utils import line_split


def get_emoji(candidate):
    party = candidate['party_id'].upper()
    emojis = CONSTANTS['emojis']
    return emojis[party] if party in emojis else emojis['UNKNOWN']


def is_contentious(race):
    return race['race_rating'].startswith('lean') or race['race_rating'] == 'tossup'


class ElectionCog(commands.Cog, name='Election'):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.output = self.bot.i18n.localize('election_loading')
        self.update = datetime.now()
        self.updater.start()

    @tasks.loop(minutes=1.0)
    async def updater(self):
        url = "https://static01.nyt.com/elections-assets/2020/data/api/2020-11-03/votes-remaining-page/national/president.json"
        localize = self.bot.i18n.localize

        async with self.bot.session.get(url, headers={"User-Agent": "semicolon-bot:v1.2"}) as r:
            if r.status != 200:
                raise WebException(self.bot.i18n, None, 'nytimes', r.status, await r.text())
            rawjson = await r.json()

        data = rawjson['data']
        races = data['races']
        full_out = []
        for race in races:
            # we only want to track contentious states (ones that lean or tossup)
            if not is_contentious(race):
                continue

            # skips races that have been called more than X hours ago
            has_winner = race['winnerCalledTimestamp'] is not None
            time_limit = timedelta(hours=2)  # how long to display recently called races
            if has_winner and (datetime.utcnow() - datetime.utcfromtimestamp(int(str(race['winnerCalledTimestamp'])[:-3]))) > time_limit:
                continue

            # get leading/losing parties
            candidates = sorted(race['candidates'], key=lambda x: x['votes'], reverse=True)
            leader = candidates[0]
            trailer = candidates[1]

            lead_symbol = get_emoji(leader)
            trail_symbol = get_emoji(trailer)

            remaining_votes = sum(map(lambda n: max(0, n['tot_exp_vote'] - n['votes']), race['counties']))
            difference = leader['votes'] - trailer['votes']
            percent_diff = leader['percent'] - trailer['percent']
            percent_needed = (((remaining_votes + difference) / 2) / remaining_votes) if remaining_votes > 0 else 0

            lead_msg = localize('election_lead_win') if leader['winner'] else localize('election_lead_tbd')
            trail_msg = localize('election_trail_lose') if leader['winner'] else localize('election_trail_tbd')
            output = [localize('election_header').format(race['state_name'], race['electoral_votes'], race['reporting_display'], remaining_votes),
                      lead_msg.format(lead_symbol, leader['name_display'], leader['votes'], leader['percent'], race['leader_margin_display']),
                      trail_msg.format(trail_symbol, trailer['name_display'], trailer['votes'], trailer['percent'], trailer['pronoun'].title(), difference+1, percent_diff, percent_needed)]
            full_out += output

        full_out.append(localize('election_footer'))
        self.output = "\n".join(full_out)
        self.update = datetime.now()

    @updater.before_loop
    async def before_updater(self):
        await self.bot.wait_until_ready()

    def cog_unload(self):
        self.updater.cancel()

    @commands.command()
    @commands.dm_only()
    async def results(self, ctx: commands.Context):
        """Command for tracking the US presidential election results of contested states"""
        update = humanize.naturaldelta(datetime.now()-self.update)
        for msg in line_split("\n".join([self.output, "", f"Last updated {update} ago"])):
            await ctx.send(msg)


def setup(bot):
    bot.add_cog(ElectionCog(bot))
