CONSTANTS = {  # meta bot data that generally shouldn't be translated or changed unless hosting your own bot
    "cog_folder": "cogs",
    "data_folder": "data",
    "lang_folder": "langs",
    "logs_folder": "logs",
    "temp_folder": "temp",
    "chip": 381941901462470658,
    "max_pokemon": 893,
    "pkmn_sprites_dir": "pkmn_sprites",
    "emojis": {  # from Bot-Moji: https://discord.gg/SyKBWjP
        "SUCCESS": ["<:greenTick:328630479886614529>", "\N{WHITE HEAVY CHECK MARK}"],
        "FAILURE": ["<:redTick:328630479576104963>", "\N{CROSS MARK}"],
        "JOIN": ["<:enter:465369329836228618>", "\N{BLACK RIGHTWARDS ARROW}"],
        "LEAVE": ["<:quit:465369329324523542>", "\N{AIRPLANE DEPARTURE}"],
        "BAN": ["<:banned:683597796955258906>", "\N{HAMMER}"],
        "UNBAN": ["<:unbanned:683597796980424734>", "\N{OPEN LOCK}"],
        "PRUNE": "\N{WARNING SIGN}",
        "RATELIMIT": ["<:refresh:465369330323030016>", "\N{CLOCK FACE THREE OCLOCK}"],
        "KICK": "\N{WOMANS BOOTS}",
        "NEW_JOIN": "\N{CLOCK FACE THREE OCLOCK} ",
        "DM_SENT": "\N{OPEN MAILBOX WITH RAISED FLAG}",
        "REPUBLICAN": "\N{LARGE RED CIRCLE}",
        "DEMOCRAT": "\N{LARGE BLUE CIRCLE}",
        "EVERYBOARD_UNUSABLE": "[?]",
        "UNKNOWN": "\N{BLACK QUESTION MARK ORNAMENT}",
        "REPORT_DELETE": ["<:delete:780290066772787221>", "\N{WASTEBASKET}"],
        "REPORT_BAN": ["<:ban:780290066798477331>", "\N{HAMMER}"],
        "REPORT_KICK": ["<:kick:780290066575785985>", "\N{WOMANS BOOTS}"],
        "REPORT_DISMISS": ["<:greenTick:328630479886614529>", "\N{WHITE HEAVY CHECK MARK}"]
    },
    "colors": {
        "default": 0x88297c,
        "message_edit": 0xf5ba5b,
        "message_delete": 0xde3d28
    },
    "everyboard_extensions": ["jpg", "png", "gif", "jpeg"],  # embeddable file types
    "pronoun_whitelist": ["she/her", "he/him", "they/them", "any pronouns", "no pronouns"],  # default/global roles
    "reddit_sort": ["hot", "new", "rising", "top", "controversial"],
    "reddit_timeable": ["top", "controversial"],
    "reddit_time": ["hour", "day", "week", "month", "year", "all"],
    "self_delete": 10,  # how long to wait before deleting approval messages
    "clear_count_warning_threshold": 100,  # highest number of messages that can be cleared before asking for confirmation
    "source_code": "https://gitlab.com/lexikiq/semicolon",
    "source_code_branch": "https://gitlab.com/lexikiq/semicolon/-/blob/master",
    "game": "{}help for help!",
    "description": '''Collection of various tools for entertainment, moderation, and more.
                      Created by lexikiq#0493 (140564059417346049)''',
    "report_emoji_path": "report_emoji.png",
    "stys_id": 144680570708951040,
    "stys_member_role": 210252019863126017,
    "quick_actions": ['DELETE', 'KICK', 'BAN', 'DISMISS'],
    "report_footer": "modmail-v1.0",  # not in i18n because this identifies modmail posts
    "audit_wait": 1.5,
    "lines_per_help_page": 20,
    "wrap_around_at": 3
}
