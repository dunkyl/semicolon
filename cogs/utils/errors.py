from __future__ import annotations  # prevents needing to make 'Localizer' a string

# avoid cyclic import
from typing import TYPE_CHECKING

from discord.ext import commands

if TYPE_CHECKING:  # evaluates to False on runtime, avoiding the import
    from cogs.utils.utils import Localizer
    from cogs.utils.bot import Semicolon


class ExpectedError(Exception):
    """Runtime errors that are expected to happen."""
    pass


class UserInputError(ExpectedError):
    """Errors caused by user input."""
    pass


class HierarchyError(UserInputError):
    """When the bot can't do something because it's out of reach."""
    pass


class SQLError(Exception):
    """Database error."""
    pass


class ConverterInitError(Exception):
    """A converter failed to initialize."""
    pass


class BaseVariableError(Exception):
    """Basic exception for variables. Anything subclassing this that isn't VariableError is considered critical."""
    pass


class CogError(ExpectedError):
    """A cog has failed to (un)load."""
    def __init__(self, cog: str, exc: Exception, bot: Semicolon, ctx: commands.Context = None):
        super().__init__(bot.i18n.localize('cog_failure', ctx=ctx).format(cog) + '\n```py\n{}\n```'.format(exc))


class VariableError(BaseVariableError, ExpectedError):
    """A variable has failed to load."""
    error = "Could not load variable {1} in {0}."

    def __init__(self, guild: int, variable: str):
        super().__init__(self.error.format(guild, variable))
        self.guild = guild
        self.variable = variable


class InvalidVariable(VariableError):
    """A variable was supplied that is not in the variable whitelist."""
    error = "The variable {1} does not exist."


class VariableParseError(VariableError, UserInputError):
    """Generic error representing a failure to parse user input."""
    error = "The variable {1} could not be parsed in {0}. Try resetting it."


class VariableMissing(VariableError):
    error = "The variable {1} could not be found in {0}."


class UnknownInterpreter(BaseVariableError):
    error = "The variable {0}'s interpreter {1!s} ({2!s}) is unknown."

    def __init__(self, interpreter, variable: str):
        super().__init__(self.error.format(variable, interpreter, type(interpreter)))


class GuildDisabledCommand(commands.CheckFailure):
    pass


class GuildExclusiveCommand(commands.CheckFailure):
    pass


class WebException(ExpectedError):
    __slots__ = {"error", "website", "status"}
    i18n_str = 'connection_failure'

    def __init__(self, i18n: Localizer, ctx: commands.Context, website: str, status: int, error: str):
        self.error = i18n.localize(self.i18n_str, ctx=ctx).format(website, status, error)
        self.website = website
        self.status = status

    def __str__(self) -> str:
        return self.error


class NoOutputFormat(ExpectedError):
    pass
