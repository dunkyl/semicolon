import logging
import os
import re
import typing
import unicodedata
from abc import ABCMeta
from datetime import datetime, timedelta
from pathlib import Path

import discord
from discord.ext import commands
from discord.utils import escape_markdown as esc_md
from whoosh.analysis import CharsetFilter, StemmingAnalyzer
from whoosh.fields import SchemaClass, TEXT, ID
from whoosh.filedb.filestore import FileStorage
from whoosh.qparser import MultifieldParser, WildcardPlugin, FieldsPlugin
from whoosh.query import FuzzyTerm
from whoosh.support.charset import accent_map
from whoosh.writing import AsyncWriter

from cogs.utils.errors import ConverterInitError, UserInputError
from cogs.utils.utils import get_yn_input, data_path, Null
from cogs.utils.variables import ContextLike

index_folder = data_path("index")
if not os.path.exists(index_folder):
    os.makedirs(index_folder)
else:
    index_expire = timedelta(days=30)

    for path, folders, files in os.walk(index_folder):
        # ignore subfolders
        if path != index_folder:
            continue
        # get all types of files based on existence of their lock file
        lock_files = filter(lambda x: x.endswith('_WRITELOCK'), files)

        for file in lock_files:
            full_path = os.path.join(path, file)

            p_obj = Path(full_path)
            dt = datetime.fromtimestamp(p_obj.stat().st_mtime)  # file modified datetime

            # if file was last modified too long ago, delete all the related files
            if (datetime.now() - dt) > index_expire:
                f_group = file[:-len("_WRITELOCK")]
                logging.getLogger("bot").info("Clearing index files for "+f_group)
                related_files = filter(lambda x: re.match(f"_?{f_group}_", x), files)
                for del_file in related_files:
                    os.remove(os.path.join(path, del_file))


return_types = typing.Union[discord.Object, discord.Guild, discord.abc.Messageable, discord.Role, discord.Emoji,
                            discord.abc.GuildChannel, Null]
class ObjectType: pass
class USER(ObjectType): pass
class MEMBER(USER): pass
class GUILD(ObjectType): pass
class CHANNEL(ObjectType): pass
class TEXT_CHANNEL(CHANNEL): pass
class VOICE_CHANNEL(CHANNEL): pass
class CATEGORY(CHANNEL): pass
class EMOJI(ObjectType): pass
class ROLE(ObjectType): pass


class BaseConverter(commands.Converter, metaclass=ABCMeta):
    def __str__(self):
        return type(self).__name__.replace('_', ' ').title()

    def get_i18n_name(self, ctx: commands.Context):
        return ctx.bot.i18n.localize('converter_'+type(self).__name__, ctx=ctx)


class ConverterWrapper(BaseConverter, metaclass=ABCMeta):
    def __init__(self, converter):
        self.converter = converter
        self.is_converter = isinstance(converter, BaseConverter)

    def __str__(self):
        return super().__str__() + ' ' + str(self.converter)

    def _get_sub_name(self, ctx: commands.Context):
        conv = self.converter
        if hasattr(conv, 'get_i18n_name'):
            return conv.get_i18n_name(ctx)
        elif isinstance(conv, type):
            return conv.__name__
        else:
            return str(conv)

    def get_i18n_name(self, ctx: commands.Context):
        return super().get_i18n_name(ctx).format(self._get_sub_name(ctx))

    async def convert_arg(self, ctx, argument, params):
        if self.is_converter:
            return await self.converter.convert(ctx, argument, params)
        else:
            try:
                return self.converter.__new__(self.converter, argument)
            except ValueError:
                raise UserInputError(str(ValueError))


class List(ConverterWrapper):
    def __init__(self, converter, separator: str = ' '):
        super().__init__(converter)
        self.separator = separator

    def __str__(self):
        f_args = (str(self.converter), self.separator, unicodedata.name(self.separator))
        return "List of {} (separated by `{}` `{}`)".format(*f_args)

    def get_i18n_name(self, ctx: commands.Context):
        f_args = (self._get_sub_name(ctx), self.separator, unicodedata.name(self.separator))
        return ctx.bot.i18n.localize('converter_'+type(self).__name__, ctx=ctx).format(*f_args)

    async def convert(self, ctx: commands.Context, argument: str, **params) -> typing.List[typing.Any]:
        return [await self.convert_arg(ctx, x, params) for x in argument.split(self.separator)]


class Nullable(ConverterWrapper):
    async def convert(self, ctx: commands.Context, argument: str, **params):
        if str(argument).lower() in ['null', 'none', 'false']:
            return Null()

        return await self.convert_arg(ctx, argument, params)


async def _confirm(ctx: commands.Context, obj, searched: typing.Set) -> bool:
    if obj.id in searched:
        return False
    searched.add(obj.id)
    obj_name = esc_md(str(obj))
    if hasattr(obj, 'mention'):
        obj_name = f"{obj.mention} ({obj_name})"
    text = ctx.bot.i18n.localize('snowflake_confirm', ctx=ctx).format(ctx.author.mention, obj_name)
    return await get_yn_input(ctx, text, delete_invoking_message=False)

analyzer = StemmingAnalyzer() | CharsetFilter(accent_map)


class ObjSchema(SchemaClass):
    snowflake = ID(stored=True, unique=True)
    name = TEXT(analyzer=analyzer, stored=True)
    nick = TEXT(analyzer=analyzer, stored=True)


class SnowflakeConvert(BaseConverter):
    snowsearch = re.compile(r"(?:<[@#:][!&]?(?:.{2,32}?:)?)?(\d{17,19})>?")
    namesearch = re.compile(r"@?(.{2,32})#(\d{4})")
    storage = FileStorage(index_folder)
    schema = ObjSchema()
    qp = MultifieldParser(["name", "nick"], schema=schema, termclass=FuzzyTerm)
    # disable some search types
    qp.remove_plugin_class(WildcardPlugin)
    qp.remove_plugin_class(FieldsPlugin)

    def __init__(self, object_type: typing.Union[ObjectType, typing.Type[ObjectType]], allow_partial: bool = True):
        if isinstance(object_type, ObjectType):
            object_type = type(object_type)
        self.object_type = object_type
        self.allow_partial = allow_partial

    def __str__(self):
        return self.object_type.__name__.replace('_', ' ').title()

    def get_i18n_name(self, ctx: commands.Context):
        return ctx.bot.i18n.localize('converter_' + type(self).__name__ + '_' + self.object_type.__name__)

    def _get_index(self, indexname: str):
        if self.storage.index_exists(indexname):
            return self.storage.open_index(indexname)
        return self.storage.create_index(self.schema, indexname=indexname)

    def obj_init(self, ctx) -> typing.Tuple[typing.Callable[[int], return_types], typing.List[return_types]]:
        if issubclass(self.object_type, MEMBER):
            get_obj = ctx.guild.get_member
            all_obj = ctx.guild.members
        elif issubclass(self.object_type, USER):
            get_obj = ctx.bot.get_user
            all_obj = ctx.bot.users
        elif issubclass(self.object_type, CHANNEL):
            get_obj = ctx.guild.get_channel
            if issubclass(self.object_type, CATEGORY):
                all_obj = ctx.guild.categories
            else:
                all_obj = getattr(ctx.guild, self.object_type.__name__.lower() + 's')
        elif issubclass(self.object_type, GUILD):
            get_obj = ctx.bot.get_guild
            all_obj = ctx.bot.guilds
        elif issubclass(self.object_type, EMOJI):
            get_obj = ctx.bot.get_emoji
            all_obj = ctx.bot.emojis
        elif issubclass(self.object_type, ROLE):
            get_obj = ctx.guild.get_role
            all_obj = ctx.guild.roles
        else:
            raise ConverterInitError("Unknown object type")
        return get_obj, all_obj

    async def convert(self, ctx: ContextLike, argument: str, ask_confirmation: bool = True):
        argument = str(argument)
        real_ctx = isinstance(ctx, commands.Context)
        if not real_ctx:
            assert ctx.guild is not None
            assert ctx.bot is not None
        else:
            await ctx.trigger_typing()

        # whether to use all_obj to find the object by ID instead (via discord.utils.get)
        alt_get = issubclass(self.object_type, (TEXT_CHANNEL, VOICE_CHANNEL, CATEGORY))

        # initialize getter objects and list objects
        get_obj, all_obj = self.obj_init(ctx)
        objname = self.object_type.__name__
        if issubclass(self.object_type, (USER, GUILD, EMOJI)):
            indexname = objname
        elif hasattr(ctx, 'guild') and ctx.guild:
            indexname = f"{objname}_{ctx.guild.id}"
        elif hasattr(ctx, 'author') and ctx.author:
            indexname = f"{objname}_{ctx.author.id}"
        else:
            indexname = None
        is_member = issubclass(self.object_type, MEMBER)  # save some slight processing
        exc = commands.BadArgument(ctx.bot.i18n.localize('snowflake_error', guild=ctx.guild).format(objname))

        if result := self.snowsearch.fullmatch(argument):
            snowflake = int(result.group(1))
            if not alt_get:
                obj = get_obj(snowflake)
            else:
                obj = discord.utils.get(all_obj, id=snowflake)
            if obj:
                return obj
            if self.allow_partial:
                return discord.Object(snowflake)
        if issubclass(self.object_type, USER) and (result := self.namesearch.fullmatch(argument)) and \
           (obj := discord.utils.get(all_obj, name=result.group(1), discriminator=result.group(2))):
            return obj

        if not ask_confirmation or not real_ctx:
            raise exc

        ctx: commands.Context
        searched: typing.Set[int] = set()

        search_str = argument
        if (issubclass(self.object_type, CHANNEL) and not issubclass(self.object_type, (VOICE_CHANNEL, CATEGORY))
            and search_str.startswith('#')) \
                or (issubclass(self.object_type, (USER, ROLE)) and search_str.startswith('@')):
            search_str = search_str[1:]
        if (obj := discord.utils.get(all_obj, name=search_str)) and await _confirm(ctx, obj, searched):
            return obj

        if is_member and (obj := discord.utils.get(all_obj, nick=search_str)) and \
                await _confirm(ctx, obj, searched):
            return obj

        # time for last resort searching!
        # one day i should properly implement incremental updates but my brain hurty rn so i just did a cheap
        #  "optimize=True" as a quick and dirty fix for increasing disk usage
        if indexname is None:
            raise exc

        ix = self._get_index(indexname)

        with ix.searcher() as searcher:
            writer = AsyncWriter(ix)
            ids = {x.id: x for x in all_obj}
            checked: typing.Set[return_types] = set()
            to_index: typing.Set[return_types] = set()

            for fields in searcher.all_stored_fields():
                sflake = int(fields['snowflake'])
                if sflake not in ids:
                    # delete
                    writer.delete_by_term('snowflake', fields['snowflake'])
                else:
                    compare = ids[sflake]
                    checked.add(compare)
                    if fields['name'] != compare.name or ('nick' in fields and hasattr(compare, 'nick') and fields['nick'] != compare.nick):
                        # update
                        to_index.add(compare)

            for _obj in all_obj:
                if _obj not in checked:
                    to_index.add(_obj)

            for _obj in to_index:
                kwargs = {"snowflake": str(_obj.id), "name": _obj.name}
                if is_member:
                    _obj: discord.Member
                    kwargs["nick"] = _obj.nick
                writer.update_document(**kwargs)
            writer.commit(optimize=True)

        # create another searcher instance as it doesn't update with new objects otherwise
        with ix.searcher() as searcher:
            q = self.qp.parse(search_str)
            results = searcher.search(q, limit=3)
            for res in results[:3]:
                if await _confirm(ctx, (obj := get_obj(int(res.get('snowflake')))), searched):
                    return obj

        # find an object whose name starts with the search query
        obj = discord.utils.find(lambda x: x.name.startswith(search_str) or
                                           (is_member and x.nick and x.nick.startswith(search_str)), all_obj)
        if obj and await _confirm(ctx, obj, searched):
            return obj

        # give up
        raise exc
