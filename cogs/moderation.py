import asyncio
import datetime
import json
import os
import re
import typing

import discord
import humanize
from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.constants import CONSTANTS
from cogs.utils.converters import SnowflakeConvert, MEMBER, ROLE, BaseConverter, List
from cogs.utils.errors import SQLError, VariableMissing, VariableParseError, VariableError, ExpectedError, \
    UserInputError, HierarchyError
from cogs.utils.sql import TableManager, GuildManager
from cogs.utils.time import get_time, unmute
from cogs.utils.utils import data_path, get_constant_emoji, get_input, shorten, add_attachment_info, is_stys, \
    comma_separator, get_yn_input, line_split
from cogs.utils.variables import FakeContext, display_variable


async def clear_reacts(message: discord.Message, emoji=None):
    chan = message.channel
    me = message.guild.me
    if chan.permissions_for(me).manage_messages:
        if emoji:
            await message.clear_reaction(emoji)
        else:
            await message.clear_reactions()
    else:
        # this is janky lol
        rc = (lambda x: x.me) if emoji is None else (lambda x: x.me and str(x) == str(emoji))
        for r in message.reactions:
            if rc(r):
                await message.remove_reaction(r, me)


class RuleManager(GuildManager):
    def __init__(self, sql, snowflake: int):
        super().__init__(sql, 'rules', ["rule text NOT NULL", "ping_here integer NOT NULL"], snowflake)


class Moderation(commands.Cog):
    disabled_cmds = data_path('disabled_commands.json')
    if not os.path.exists(disabled_cmds):
        with open(disabled_cmds, 'w') as f:
            f.write('{}')

    quick_actions = CONSTANTS['quick_actions']
    report_match = re.compile(r"https://discord.com/channels/(\d{17,19})/(\d{17,19})/(\d{17,19})")
    id_match = re.compile(r"(\d{17,19})")

    mod_config_options = {
        "muted_role": SnowflakeConvert(ROLE, allow_partial=False),
        "join_role": SnowflakeConvert(ROLE, allow_partial=False),
        "prefix": List(str)
    }

    def __init__(self, bot: Semicolon):
        self.bot = bot
        self._ = bot.i18n.localize
        self._p = bot.i18n.localize_p

        for k, v in self.mod_config_options.items():
            self.bot.config_options[k] = v

    async def add_join_role(self, member: discord.Member):
        """Adds the default role to a user"""
        guild: discord.Guild = member.guild
        ctx = FakeContext(bot=self.bot, guild=guild)

        try:
            # find if server has a join role
            role: discord.Role = await self.bot.get_variable(ctx, "join_role", False)
        except VariableError as _:
            return

        # add role
        if role and role not in member.roles and role < guild.me.top_role:
            await member.add_roles(role, reason=self.bot.i18n.localize("join_role_reason", guild=guild))

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        """Add role on joining server"""
        guild: discord.Guild = member.guild

        # ignore users who have not consented to rules, or if self cannot manage roles
        if member.pending or not guild.me.guild_permissions.manage_roles:
            return
        await self.add_join_role(member)

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        """Add role on finishing reading rules"""
        guild: discord.Guild = after.guild

        # only get users who just finished reading rules
        if after.pending or not before.pending or not guild.me.guild_permissions.manage_roles:
            return
        await self.add_join_role(after)

    def get_rules_manager(self, guild):
        if isinstance(guild, discord.Guild):
            guild = guild.id
        return RuleManager(self.bot.sql, guild)

    def get_action_emoji(self, key: str, modmail: discord.TextChannel, message: discord.Message)\
            -> typing.Union[str, None]:
        key = key.upper()
        channel: discord.TextChannel = message.channel
        guild: discord.Guild = message.guild
        me = guild.me
        perms: discord.Permissions = channel.permissions_for(me)
        if key == 'DELETE':
            if not perms.manage_messages:
                return
        elif key == 'BAN':
            if not perms.ban_members:
                return
        elif key == 'KICK':
            if not perms.kick_members:
                return
        elif key not in self.quick_actions:
            raise ValueError(f"Invalid quick-action emoji {key}")
        return get_constant_emoji('REPORT_'+key, channel=modmail)

    async def channel_setup(self, ctx: commands.Context, constants_key: str) -> str:
        cname = self.bot.bot_config['channels'][constants_key]
        existing_channel = discord.utils.get(ctx.guild.text_channels, name=cname)
        if existing_channel:
            return self.bot.i18n.localize('channel_fail', ctx=ctx).format(existing_channel)
        create_in = ctx.guild.get_channel(ctx.channel.category_id) if ctx.channel.category_id else ctx.guild
        overwrites = {
            ctx.guild.default_role: discord.PermissionOverwrite(send_messages=False),
            ctx.guild.me: discord.PermissionOverwrite(send_messages=True, read_messages=True)
        }
        new_chan = await create_in.create_text_channel(cname, topic=self._(constants_key + '_topic', guild=ctx.guild),
                                                       overwrites=overwrites,
                                                       reason=self._('reason', guild=ctx.guild).format(ctx.author))
        self.bot.config_set(ctx.guild.id, constants_key+"_channel", new_chan.id)
        return self.bot.i18n.localize('channel_created', ctx=ctx).format(new_chan)

    def get_rules(self, guild: typing.Union[int, discord.Guild]) -> list:
        rules = self.get_rules_manager(guild).get_rows()
        if not rules:
            return []

        return [[x[0], bool(x[1])] for x in rules]

    async def process_report(self, reactpayload: discord.RawReactionActionEvent):
        gid: int = reactpayload.guild_id
        cid: int = reactpayload.channel_id
        mid: int = reactpayload.message_id
        emoji: discord.PartialEmoji = reactpayload.emoji
        reporter: discord.Member = reactpayload.member
        guild: discord.Guild = self.bot.get_guild(gid)
        channel: discord.TextChannel = guild.get_channel(cid)
        message: discord.Message = await channel.fetch_message(mid)
        author: discord.Member = message.author
        kwargs = {'guild': guild.id, 'channel': channel.id, 'user': reporter.id}
        _ = self.bot.i18n.localize

        if channel.permissions_for(guild.me).manage_messages:
            await message.remove_reaction(emoji, reporter)
        if author == reporter:
            return
        modmail: discord.TextChannel = await self.bot.get_join_channel(guild, "modmail")
        if modmail is None:
            return

        mperm: discord.Permissions = modmail.permissions_for(guild.me)
        if not (mperm.read_messages and mperm.send_messages and mperm.add_reactions and mperm.embed_links):
            # bot is missing permissions, but we should only report the error if the report emoji is from the server
            # (ie if the server has setup reporting)
            femoji: discord.Emoji
            if (femoji := self.bot.get_emoji(emoji.id)) and femoji.guild_id == gid:
                try:
                    owner: discord.Member = guild.owner
                    gname = discord.utils.escape_markdown(guild.name)
                    msg = _('report_owner_error', guild=guild.id, user=owner.id).format(gname, modmail.mention, emoji)
                    await owner.send(msg)
                except discord.Forbidden:
                    pass
            return

        raw_rules = self.get_rules(guild)
        items = [_('report_dm_cancel', **kwargs)] + [r[0] for r in raw_rules] + [_('report_dm_other', **kwargs)]
        selections = [None] + raw_rules + [[_('report_dm_other', channel=modmail, guild=guild), False]]

        rdat = await get_input(title=_('report_dm_title', **kwargs),
                               prompt=_('report_dm_prompt', **kwargs).format(message.jump_url),
                               items=items, selections=selections, bot=self.bot,
                               guild=guild, channel=channel, dest=reporter, author=reporter)
        if rdat is None:
            return
        rule = rdat[0]
        ping = rdat[1]
        mcontent = '@here' if ping else None
        # i don't think this is necessary, but just to be safe:
        mentions = discord.AllowedMentions.none()
        mentions.everyone = ping

        kwargs = {'guild': guild.id, 'channel': modmail.id}

        embed = discord.Embed(title=shorten(_('report_modmail_title', **kwargs).format(channel), 256),
                              color=CONSTANTS['colors']['message_delete'],
                              description=_('report_modmail_desc', **kwargs).format(message.jump_url),
                              timestamp=message.created_at)
        # display reporter
        embed.add_field(name=_('report_modmail_reporter_title', **kwargs),
                        value=_('report_modmail_user_desc', **kwargs).format(reporter), inline=False)
        # display reported user
        embed.add_field(name=_('report_modmail_user_title', **kwargs),
                        value=_('report_modmail_user_desc', **kwargs).format(author), inline=False)
        # display reported rule
        embed.add_field(name=_('report_modmail_rule_title', **kwargs), value=rule, inline=False)
        # get message contents
        if message.system_content:
            content = shorten(message.system_content, 1024)
        else:
            content = _('message_blank_content', channel=modmail, guild=guild)
        # display message content
        embed.add_field(name=_('report_modmail_post_title', **kwargs), value=content, inline=False)
        # set footer
        embed.set_footer(text=CONSTANTS['report_footer'], icon_url=self.bot.user.avatar_url_as(format='png', size=128))
        # add attached image
        embed = add_attachment_info(embed, message, self.bot.i18n)

        mmmsg: discord.Message = await modmail.send(mcontent, embed=embed, allowed_mentions=mentions)
        # add quick-action emojis
        emojis = filter(lambda x: x, map(lambda x: self.get_action_emoji(x, modmail, message), self.quick_actions))
        for emoji in emojis:
            await mmmsg.add_reaction(emoji)

    async def quick_action(self, reactpayload: discord.RawReactionActionEvent):
        gid: int = reactpayload.guild_id
        cid: int = reactpayload.channel_id
        mid: int = reactpayload.message_id
        emoji: discord.PartialEmoji = reactpayload.emoji
        moderator: discord.Member = reactpayload.member
        guild: discord.Guild = self.bot.get_guild(gid)
        channel: discord.TextChannel = guild.get_channel(cid)
        if channel != await self.bot.get_join_channel(guild, 'modmail'):
            return  # not the modmail channel
        message: discord.Message = await channel.fetch_message(mid)
        author: discord.Member = message.author
        kwargs = {'guild': guild.id, 'channel': channel.id, 'user': moderator.id}
        _ = self.bot.i18n.localize
        _p = self.bot.i18n.localize_p

        # ensure reacted message is a modmail message
        if author.id != self.bot.user.id:
            return  # not my post, ignore it
        if not message.embeds:
            return
        embed = message.embeds[0]
        if embed.footer is discord.Embed.Empty:
            return
        if embed.footer.text != CONSTANTS['report_footer']:
            return
        if (match := self.report_match.search(embed.description)) is None:
            return
        r_cid, r_mid = map(int, match.group(2, 3))

        # get action
        action = None
        for key in self.quick_actions:
            if str(emoji) in CONSTANTS['emojis']['REPORT_'+key]:
                action = key
                break
        if action is None:
            # not an action emoji
            return

        # get info of reported post
        r_chan: discord.TextChannel = guild.get_channel(r_cid)

        # message & author
        r_auth_id = int(self.id_match.search(embed.fields[1].value).group(1))
        try:
            r_msg: discord.Message = await r_chan.fetch_message(r_mid)
            r_author = r_msg.author
            r_auth_id = r_author.id
        except discord.NotFound:
            r_msg = None
            r_author = guild.get_member(r_auth_id)

        uperms: discord.Permissions = r_chan.permissions_for(moderator)
        perms: discord.Permissions = r_chan.permissions_for(guild.me)
        # run action
        reason = _('report_action_reason', guild=guild).format(moderator)
        if action == 'DISMISS':
            await clear_reacts(message)
        elif action == 'DELETE':
            if uperms.manage_messages and perms.manage_messages:
                if r_msg:
                    await r_msg.delete()
                await clear_reacts(message, emoji)
        elif action == 'BAN':
            if uperms.ban_members and perms.ban_members:
                days = list(range(1, 8))+[0]
                items = [_p('report_ban_days', x, **kwargs).format(x) for x in days] + [_('get_input_cancel', **kwargs)]
                days.append(None)
                del_days = await get_input(title=_('report_ban_title', **kwargs), prompt=_('report_ban_desc', **kwargs),
                                           items=items, selections=days,
                                           guild=guild, channel=channel, author=moderator, bot=self.bot)
                if del_days is not None:
                    if r_auth_id:
                        await guild.ban(discord.Object(r_auth_id), reason=reason, delete_message_days=del_days)
                    await clear_reacts(message, emoji)
        elif action == 'KICK':
            if uperms.kick_members and perms.kick_members:
                if r_author:
                    await r_author.kick(reason=reason)
                await clear_reacts(message, emoji)
        else:
            raise NotImplementedError("Reached invalid state with emoji {} and action {}".format(str(emoji), action))

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, reactpayload: discord.RawReactionActionEvent):
        _ = self.bot.i18n.localize
        # ignore DM reactions
        if not (gid := reactpayload.guild_id):
            return
        emoji: discord.PartialEmoji = reactpayload.emoji
        member: discord.Member = reactpayload.member
        if member is None:
            return
        if member.id == self.bot.user.id:  # ignore self obv
            return

        guild: discord.Guild = self.bot.get_guild(gid)
        if emoji.is_custom_emoji():
            if emoji.name == _('report_emoji', guild=guild):
                return await self.process_report(reactpayload)
            # weird hacky thing to get the custom emojis lmfao
            cemojis = []
            for qa_emoji_key in self.quick_actions:
                for qa_emoji in CONSTANTS['emojis']['REPORT_'+qa_emoji_key]:
                    cemojis.append(qa_emoji)
            if str(emoji) in cemojis:
                return await self.quick_action(reactpayload)

    @commands.group(invoke_without_command=True, aliases=['rule'])
    @commands.guild_only()
    async def rules(self, ctx: commands.Context, rule_number: int = None):
        """
        Lists the servers rules.
        Moderators can add rules to be displayed when reporting a message (see `;help setup reports`)
        """
        rules = self.get_rules(ctx.guild)
        if not rules:
            await ctx.send(self._('rules_none', ctx=ctx))
            return

        if rule_number:
            if len(rules) <= rule_number:
                out = self._('rules_get', ctx=ctx).format(ctx.author.mention, rules[rule_number-1])
            else:
                out = self._('rules_missing', ctx=ctx).format(ctx.author.mention, rule_number)
            await ctx.send(out)
            return

        rules = [self._('rules_list', ctx=ctx).format(c, rule[0]) for c, rule in enumerate(rules, 1)]
        rules.append(self._('rules_requested', ctx=ctx).format(ctx.author.mention))
        for m in line_split('\n'.join(rules)):
            await ctx.send(m)

    @rules.command(name='add', aliases=['create'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_add(self, ctx: commands.Context, *, rule: str):
        """
        Adds a new rule to the server.
        """
        self.get_rules_manager(ctx.guild).create_row((rule, int(False)))
        await ctx.send(self._('rules_add', ctx=ctx).format(ctx.author.mention))

    @rules.command(name='set', aliases=['modify', 'update'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_set(self, ctx: commands.Context, rule_number: int, *, rule: str):
        """
        Modifies an existing server rule.
        """
        try:
            key = 'success'
            self.get_rules_manager(ctx.guild).edit_row(rule_number, (rule,), ['rule'])
        except (SQLError, IndexError):
            key = 'failure'
        await ctx.send(self._(f'rules_set_{key}', ctx=ctx).format(ctx.author.mention, rule_number))

    @rules.command(name='remove', aliases=['delete'])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_remove(self, ctx: commands.Context, rule_number: int):
        """
        Removes a server rule.
        """
        try:
            key = 'success'
            self.get_rules_manager(ctx.guild).delete_row(rule_number)
        except (SQLError, IndexError):
            key = 'failure'
        await ctx.send(self._(f'rules_rem_{key}', ctx=ctx).format(ctx.author.mention, rule_number))

    @rules.group(name='ping', aliases=['alert'], invoke_without_command=True)
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def r_ping(self, ctx: commands.Context, rule_number: int, toggle: bool = None):
        """
        Toggles if a rule should ping @here when reported.
        """
        try:
            if toggle is None:
                toggle = not(bool(self.get_rules(ctx.guild)[rule_number-1][1]))
            key = f'success_{str(toggle)}'
            self.get_rules_manager(ctx.guild).edit_row(rule_number, (int(toggle),), ['ping_here'])
        except (SQLError, IndexError):
            key = 'failure'
        await ctx.send(self._(f'rules_ping_{key}', ctx=ctx).format(ctx.author.mention, rule_number))

    @r_ping.command(name='list', invoke_without_command=True)
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def rp_list(self, ctx: commands.Context):
        """
        Lists rules that will ping @here when reported.
        """
        rules = self.get_rules(ctx.guild)
        rules = [self._('rules_list', ctx=ctx).format(c, rule[0]) for c, rule in enumerate(rules, 1) if rule[1]]
        if not rules:
            await ctx.send(self._('rules_ping_none', ctx=ctx).format(ctx.author.mention))
            return
        rules.append(self._('rules_requested_short', ctx=ctx).format(ctx.author.mention))
        for m in line_split('\n'.join(rules)):
            await ctx.send(m)

    @rules.command(name='import')
    @commands.guild_only()
    @commands.is_owner()
    async def r_import(self, ctx: commands.Context):
        cols = ["rule text NOT NULL", "ping_here integer NOT NULL"]
        temp = TableManager(self.bot.sql, f'rules_{ctx.guild.id}', cols)
        for row in temp.get_rows():
            self.get_rules_manager(ctx.guild).create_row(row)
        temp.delete()

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def setup(self, ctx: commands.Context):
        """Sets up a semicolon feature on a server."""
        await ctx.send_help(ctx.command)

    @setup.command(name='joinlog', brief='Creates a channel that logs joins, leaves, & name changes.')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_joinlog(self, ctx: commands.Context):
        """Creates a channel that logs member joins, leaves, bans, unbans, name changes, and discriminator changes."""
        await ctx.send(await self.channel_setup(ctx, 'joins'))

    @setup.command(name='messagelog')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_messages(self, ctx: commands.Context):
        """Creates a channel that logs edited and deleted messages."""
        await ctx.send(await self.channel_setup(ctx, 'messages'))

    @setup.command(name='pronouns')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def setup_pronouns(self, ctx: commands.Context):
        """Sets up automatic, cross-server pronoun roles."""
        toprole = ctx.guild.me.top_role
        for pronoun in CONSTANTS['pronoun_whitelist']:
            p_name = self.bot.bot_config['pronoun_prefix'] + pronoun
            p_role = discord.utils.find(lambda r: r.name == p_name and toprole > r, ctx.guild.roles)
            if not p_role:
                await ctx.guild.create_role(name=self.bot.bot_config['pronoun_prefix'] + pronoun)
                # pronoun cog will detect this role creation and create the rest. so, to avoid race conditions,
                break
        await asyncio.sleep(.1)
        await ctx.send(self.bot.i18n.localize('pronouns_created', ctx=ctx))

    @setup.command(name='reports', aliases=['report', 'reporting'])
    @commands.has_guild_permissions(manage_channels=True, manage_emojis=True)
    @commands.bot_has_guild_permissions(manage_channels=True, manage_emojis=True, manage_messages=True)
    async def setup_reports(self, ctx: commands.Context):
        """
        Adds an emoji for users to report posts and a channel to log the reports.
        Upon seeing a rule-breaking post, users can react with the added emoji and it will be sent to the modmail channel for moderators to take action.
        """
        # short vars
        guild: discord.Guild = ctx.guild
        me: discord.Member = guild.me
        _ = self.bot.i18n.localize

        # restrict report access to only members on stys
        roles = None
        if is_stys(ctx, False):
            roles = [guild.get_role(CONSTANTS['stys_member_role'])]

        # create emoji
        ename = _('report_emoji', guild=guild)  # emoji name
        if (emoji := discord.utils.get(guild.emojis, name=ename)) is None:  # if emoji doesn't already exist
            try:
                with open(CONSTANTS['report_emoji_path'], 'rb') as f:
                    emoji = await guild.create_custom_emoji(name=ename, image=f.read(), roles=roles)
            except discord.HTTPException as e:
                raise ExpectedError(str(e))

        # create text channel
        cname = self.bot.bot_config['channels']['modmail'].lower()  # channel name
        if (channel := discord.utils.get(guild.text_channels, name=cname)) is None:  # if channel doesn't already exist
            dest = guild.get_channel(ctx.channel.category_id) if ctx.channel.category_id else guild
            if not dest:
                dest = guild

            # set basic text channel overwrites
            overwrites = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                guild.me: discord.PermissionOverwrite(manage_messages=True, read_messages=True,
                                                      use_external_emojis=True,
                                                      add_reactions=True, read_message_history=True)
            }

            # allow mods to access channel
            for role in guild.roles:
                perms = role.permissions
                if (perms.kick_members and perms.manage_messages) or perms.administrator:
                    overwrites[role] = discord.PermissionOverwrite(read_messages=True)

            # attempt creation
            try:
                channel = await dest.create_text_channel(cname, overwrites=overwrites,
                                                         topic=_('modmail_topic', guild=guild))
            except discord.HTTPException as e:
                raise ExpectedError(str(e))

        # generate output
        out = [_('report_setup_out', ctx=ctx).format(emoji, channel.mention, ctx.prefix)]
        # warn of channels semicolon can't manage
        chans = [x.mention for x in guild.text_channels if not me.permissions_in(x).manage_messages and me.permissions_in(x).read_messages]
        # warn of missing guild permissons
        perms: discord.Permissions = guild.me.guild_permissions
        kick = [_('report_setup_out_kick', ctx=ctx)] if not perms.kick_members else []
        ban = [_('report_setup_out_ban', ctx=ctx)] if not perms.ban_members else []
        # add outputs
        if chans or kick or ban:
            out.append(_('report_setup_out_consider', ctx=ctx))
            if chans:
                out.append(_('report_setup_out_channels', ctx=ctx).format(comma_separator(chans)))
            out += kick
            out += ban

        # send output
        await ctx.send('\n'.join(out))

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def toggle(self, ctx: commands.Context, command: str, action: bool):
        """
        Enables or disables a command for the current guild.
        """
        cmd = ctx.bot.get_command(command)

        if cmd is None:
            await ctx.send(self.bot.i18n.localize('toggle_no_command', ctx=ctx))
            return

        while cmd.parent is not None:  # get the base command
            cmd = cmd.parent

        if cmd.name in ['toggle']:
            await ctx.send(self.bot.i18n.localize('toggle_no_toggle_toggle', ctx=ctx))
            return

        with open(self.disabled_cmds) as j:
            data = json.load(j)
        json_key = str(ctx.guild.id)  # json can't save ints as keys
        if json_key not in data:
            data[json_key] = []
        gdata = data[json_key]

        langkey = ["toggle"]

        if not action:
            tkey = 'failure'
            if cmd.name not in gdata:
                gdata.append(cmd.name)
                tkey = 'success'

            langkey += [tkey, 'disable']
        else:
            tkey = 'failure'
            while cmd.name in gdata:  # just incase it duped?? idk
                gdata.remove(cmd.name)
                tkey = 'success'
            if len(gdata) == 0:
                del data[json_key]

            langkey += [tkey, 'enable']

        with open(self.disabled_cmds, 'w') as j:
            json.dump(data, j)

        await ctx.send(self.bot.i18n.localize('_'.join(langkey), ctx=ctx).format(ctx.prefix, cmd.name, ctx.guild.name))

    @commands.group(aliases=['purge', 'delete'], invoke_without_command=True)
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    @commands.guild_only()
    async def clear(self, ctx, messages: int):
        """
        Deletes X amount of messages.
        X does not include the message invoking the command.
        """
        warning_limit = CONSTANTS['clear_count_warning_threshold']

        if messages <= 0:
            raise UserInputError(self.bot.i18n.localize('clear_count_too_low', ctx=ctx))

        messages = messages+1
        if messages <= warning_limit or await get_yn_input(ctx, self.bot.i18n.localize('clear_large_count_warning', ctx=ctx).format(warning_limit)):
            await ctx.channel.purge(limit=messages)

    @clear.command(name='after')
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    @commands.guild_only()
    async def clear_after(self, ctx: commands.Context, message_id: int, upto_id: int = None):
        """
        Clears messages sent after a provided message ID.
        The provided message ID is *exclusive*, the optional upto ID is *inclusive*.
        Command works by extracting the time from the ID and deleting anything afterwards, so any ID is valid input.
        Will ask for confirmation if you try to clear over 1 days worth of messages.
        """
        dtstop = discord.utils.snowflake_time(message_id)  # gets datetime object corresponding to the input
        dtstrt = None
        if upto_id is not None:
            dtstrt = discord.utils.snowflake_time(upto_id) - datetime.timedelta(seconds=.001)

        async def the_purge():
            await ctx.channel.purge(after=dtstop, before=dtstrt, limit=None)
            if dtstrt and dtstrt <= ctx.message.created_at:
                try:
                    await ctx.message.delete()
                except (discord.HTTPException, discord.Forbidden):
                    pass

        if ((datetime.datetime.now() - dtstop) <= datetime.timedelta(days=1)) or await get_yn_input(ctx, self.bot.i18n.localize('clear_after_warning', ctx=ctx)):
            await the_purge()

    @commands.command(aliases=['hackban'])
    @commands.bot_has_permissions(ban_members=True)
    @commands.has_permissions(ban_members=True)
    @commands.guild_only()
    async def ban(self, ctx: commands.Context, user: SnowflakeConvert(MEMBER),
                                               days_to_delete: typing.Optional[int] = 0,
                                               *reason: str):
        """(Hack)bans a user from a server. Accepts IDs and fuzzy search."""
        user: discord.Member
        if hasattr(user, 'top_role') and user.top_role >= ctx.me.top_role:
            raise HierarchyError(self.bot.i18n.localize("ban_hierarchy", ctx=ctx))
        if not (0 <= days_to_delete <= 7):
            raise ValueError(self.bot.i18n.localize("invalid_range", ctx=ctx)
                             .format(days_to_delete, "days_to_delete", 0, 7))
        guild: discord.Guild = ctx.guild
        reason_s = ' '.join(reason)
        fmt = (f"{str(ctx.author)} ({ctx.author.id})", ctx.prefix, ctx.invoked_with)
        reason_s += self.bot.i18n.localize("hackban_reason", guild=guild).format(*fmt)
        reason_s = shorten(reason_s.strip(), 512)
        await guild.ban(user, reason=reason_s, delete_message_days=days_to_delete)
        await ctx.send(self.bot.i18n.localize("hackban", ctx=ctx).format(user.id))

    @commands.group(invoke_without_command=True)
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def config(self, ctx: commands.Context):
        """Change guild variables."""
        await ctx.send_help(ctx.command)

    @staticmethod
    def option_type(ctx: commands.Context, value, key: str):
        if isinstance(value, BaseConverter):
            text = value.get_i18n_name(ctx)
        else:
            text = type(value).__name__.replace('_', ' ').title()
        text = f"({text})"
        key = 'note_'+key
        if (note := ctx.bot.i18n.localize(key, ctx=ctx)) != f"[{key}]":
            text += f"\n  *\\* {note}*"
        return text

    @config.command(name="list")
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def config_list(self, ctx: commands.Context):
        """Lists all available config options."""
        opts = [f"- `{k}` {self.option_type(ctx, v, k)}" for k, v in self.bot.config_options.items()]
        opts.insert(0, self.bot.i18n.localize("config_list", ctx=ctx))
        await ctx.send("\n".join(opts))

    @config.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def get(self, ctx: commands.Context, variable: str):
        """Gets the current value of a variable."""
        p_data = display_variable(await self.bot.get_variable(ctx, variable))
        await ctx.send(self.bot.i18n.localize("get_variable", ctx=ctx).format(variable, p_data))

    @config.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def set(self, ctx: commands.Context, variable: str, *, data: str):
        """Sets the value of a variable."""
        variable = self.bot.validate_variable(variable)
        try:
            p_data = display_variable(await self.bot.set_variable(ctx, variable, data))
            await ctx.send(self.bot.i18n.localize("set_variable", ctx=ctx).format(variable, p_data))
        except VariableParseError:
            await ctx.send(self.bot.i18n.localize("set_variable_parse_error", ctx=ctx))

    @config.command(name='clear')
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def config_clear(self, ctx: commands.Context, variable: str):
        """Clears (removes) the value of a variable."""
        variable = self.bot.validate_variable(variable)
        self.bot.config_clear(ctx.guild.id, variable)
        await ctx.send(self.bot.i18n.localize("clear_variable", ctx=ctx).format(variable))

    @commands.command()
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def mute(self, ctx, member: SnowflakeConvert(MEMBER), *, time: get_time):
        """Mutes a user for a specified duration."""
        member: discord.Member
        try:
            mute_role: discord.Role = await self.bot.get_variable(ctx, "muted_role")
            if mute_role >= ctx.guild.me.top_role:
                await ctx.send(self.bot.i18n.localize("mute_too_high", ctx=ctx))
                return
        except VariableMissing:
            await ctx.send(self.bot.i18n.localize("no_mute_role", ctx=ctx))
            return

        args = (member.id, ctx.guild.id, mute_role.id)
        self.bot.add_task(time, unmute, *args)

        human_time = humanize.naturaldelta(time-datetime.datetime.utcnow())
        if mute_role not in member.roles:
            reason = self.bot.i18n.localize("mute_reason", guild=ctx.guild).format(human_time, ctx.author)
            await member.add_roles(mute_role, reason=reason)
        await ctx.send(self.bot.i18n.localize("muted", ctx=ctx).format(member, human_time))


def setup(bot):
    bot.add_cog(Moderation(bot))
