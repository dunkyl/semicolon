import datetime
import inspect
import os
import random
import re
import typing
import uuid

import discord
import humanize
from discord.ext import commands
from discord.utils import escape_markdown as esc_md
from youtube_dlc import YoutubeDL, DownloadError

from cogs.utils.bot import Semicolon
from cogs.utils.constants import CONSTANTS
from cogs.utils.errors import WebException, UserInputError, NoOutputFormat, ExpectedError
from cogs.utils.time import get_time
from cogs.utils.time import remind as reminder
from cogs.utils.utils import embed_author_template, shorten


class DummyLogger:
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        pass


def quality_filter(fmt, max_size: int, extractor: str) -> bool:
    if 'filesize' not in fmt or not fmt['filesize'] or fmt['filesize'] > max_size:
        # or 'DASH' in str(fmt['format_note']) or ' only ' in fmt['format']:
        return False
    if extractor == 'youtube' and (fmt['vcodec'] == 'none' or fmt['acodec'] == 'none'):
        return False
    return True


class Miscellaneous(commands.Cog):
    re_roll = re.compile(r"[a-z0-9 ,:=+*]+", re.I)

    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.ytdl_opts = {
            # "outtmpl": os.path.join(CONSTANTS['temp_folder'], "%(id)s.%(ext)s"),
            "quiet": True,
            "no_warnings": True,
            "fixup": "detect_or_warn",
            "logger": DummyLogger()
        }
        self.ytdl = YoutubeDL(self.ytdl_opts)

    @commands.command(aliases=['pong', 'pog', 'pingpong'])
    @commands.cooldown(rate=1, per=3, type=commands.BucketType.channel)
    async def ping(self, ctx: commands.Context):
        """Is the bot alive? If you're reading this, probably."""
        unit = 'microseconds'  # can also be 'milliseconds', 'seconds'

        cmdstr = ctx.invoked_with
        cmdstr = cmdstr[0].upper() + cmdstr[1:] + '!'
        output = [cmdstr]
        msg = await ctx.send(output[0])

        delay = msg.created_at - ctx.message.created_at
        output.append(self.bot.i18n.localize('ping1', ctx=ctx).format(humanize.naturaldelta(delay, minimum_unit=unit)))
        ltncy = datetime.timedelta(seconds=ctx.bot.latency)
        output.append(self.bot.i18n.localize('ping2', ctx=ctx).format(humanize.naturaldelta(ltncy, minimum_unit=unit)))

        await msg.edit(content='\n'.join(output))

    @commands.command()
    @commands.is_owner()
    async def echo(self, ctx: commands.Context, *, text: str):
        await ctx.send(text)

    @commands.command()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    @commands.bot_has_permissions(embed_links=True)
    async def reddit(self, ctx: commands.Context, subreddit: str, limit: int = 100, sort: str = "hot", time: str = "day"):
        """Grabs a recent image from the specified subreddit.
        'limit' is an integer upto 100 (the default) that sets the limit for the number of posts to get and pick from.
        'sort' can be one of 'hot', 'new', 'rising', 'top', 'controversial'
        'time' is used with 'top' and 'controversial' and can be one of 'hour', 'day', 'week', 'month', 'year', 'all'"""
        async with ctx.typing():
            limit = max(1, min(100, limit))
            sort = sort.lower()
            time = time.lower()
            # input validation
            if sort not in CONSTANTS['reddit_sort']:
                await ctx.send(self.bot.i18n.localize('invalid_argument', ctx=ctx).format('sort', '`, `'.join(CONSTANTS['reddit_sort'])))
                return
            url = 'https://www.reddit.com/r/{0}/{1}/.json?count={2}&limit={2}'.format(subreddit, sort, limit)
            if sort in CONSTANTS['reddit_timeable']:
                if time not in CONSTANTS['reddit_time']:
                    await ctx.send(self.bot.i18n.localize('invalid_argument', ctx=ctx).format('time', '`, `'.join(CONSTANTS['reddit_time'])))
                    return
                url += f'&t={time}'

            async with ctx.bot.session.get(url, headers={"User-Agent": "semicolon-bot:v1.1 (/u/noellekiq)"}) as r:
                if r.status != 200:
                    raise WebException(self.bot.i18n, ctx, 'reddit', r.status, await r.text())
                jsondata = await r.json()

            if 'error' in jsondata:
                raise ExpectedError(self.bot.i18n.localize('reddit_data_failed', ctx=ctx))

            posts = jsondata['data']['children']
            if isinstance(ctx.channel, discord.TextChannel):
                includensfw = ctx.channel.is_nsfw()
            else:
                includensfw = True

            viableposts = []
            for post in posts:
                pdata = post['data']
                is_safe = not pdata['over_18'] or (pdata['over_18'] and includensfw)
                if "post_hint" in pdata and pdata['post_hint'] == 'image' and is_safe:
                    viableposts.append(pdata)

            if not viableposts:
                raise ExpectedError(self.bot.i18n.localize('reddit_no_posts', ctx=ctx))

            pdata = random.choice(viableposts)
            embed = embed_author_template(ctx, True)
            embed.title = shorten(pdata['title'], 256)
            embed.description = self.bot.i18n.localize('reddit_byline', ctx=ctx).format(pdata['author'], pdata['subreddit'], 'https://reddit.com'+pdata['permalink'])
            embed.set_image(url=pdata['url'])
            await ctx.send(embed=embed)

    @commands.command(name='count')
    @commands.is_owner()
    async def cmd_count(self, ctx: commands.Context, user: typing.Union[discord.User, int], *, regex: str):
        """Counts messages containing regex from a user."""
        async with ctx.typing():
            if not isinstance(user, int):
                uid = user.id
            else:
                uid = user
            re_search = re.compile(regex, re.IGNORECASE)
            if isinstance(ctx.channel, discord.DMChannel):
                chans: typing.List[discord.DMChannel] = [ctx.channel]
            else:
                def canview(channel: discord.TextChannel, member: discord.Member):
                    perms = channel.permissions_for(member)
                    return perms.read_messages and perms.read_message_history
                chans: typing.Generator[discord.TextChannel] = (x for x in ctx.guild.text_channels if canview(x, ctx.guild.me))
            count = 0
            for chan in chans:
                async for msg in chan.history(limit=None):
                    if msg.author.id != uid:
                        continue
                    count += re_search.findall(msg.content)
            await ctx.send(self.bot.i18n.localize('count_cmd', ctx=ctx).format(count, user, regex, len(chans)))

    @commands.command(aliases=['add', 'join'])
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    async def invite(self, ctx):
        """Add me to your server!"""
        await ctx.send(self.bot.i18n.localize('invite', ctx=ctx).format(self.bot.invite_url))

    @commands.command()
    async def source(self, ctx, *, command: str = None):
        """Displays the source for a command."""
        if command is None:
            return await ctx.send(CONSTANTS['source_code'])

        obj = ctx.bot.get_command(command.replace('.', ' '))
        if obj is None:
            return await ctx.send(self.bot.i18n.localize('source_missing', ctx=ctx))

        # since we found the command we're looking for, presumably anyway, let's try to access the code itself
        src = obj.callback.__code__
        lines, firstlineno = inspect.getsourcelines(src)
        if not obj.callback.__module__.startswith('discord'):
            # not a built-in command
            location = os.path.relpath(src.co_filename).replace('\\', '/')
            source_url = CONSTANTS['source_code_branch']
        else:
            location = obj.callback.__module__.replace('.', '/') + '.py'
            source_url = 'https://github.com/Rapptz/discord.py/blob/rewrite'

        final_url = f'<{source_url}/{location}#L{firstlineno}-{firstlineno + len(lines) - 1}>'
        await ctx.send(final_url)

    @commands.command()
    async def remind(self, ctx, time: get_time, *, message: str):
        """Reminds you to do something in a specified amount of time."""
        # TODO: remove need to use quotes for the time argument (check robo danny source?)
        args = (ctx.author.id, ctx.message.id, message)
        self.bot.add_task(time, reminder, *args)

        now = datetime.datetime.utcnow().replace(microsecond=0)
        human_time = humanize.naturaltime(time, when=now)
        await ctx.send(self.bot.i18n.localize("remind_me", ctx=ctx).format(human_time))

    async def random_animal(self, url: str, field: str) -> str:
        return (await self.bot.get_api_json(url))[field]

    @commands.command()
    async def cat(self, ctx: commands.Context):
        """Posts a random image of a cat"""
        await ctx.send(await self.random_animal("aws.random.cat/meow", "file"))

    @commands.command()
    async def dog(self, ctx: commands.Context):
        """Posts a random image of a dog"""
        await ctx.send(await self.random_animal("random.dog/woof.json", "url"))

    @commands.command()
    async def fox(self, ctx: commands.Context):
        """Posts a random image of a fox"""
        await ctx.send(await self.random_animal("randomfox.ca/floof", "image"))

    @commands.command()
    async def pug(self, ctx: commands.Context):
        """Posts a random image of a pug"""
        await ctx.send(await self.random_animal("randompug.club/loaf", "image"))

    @commands.command(name="roll")
    async def roll_cmd(self, ctx: commands.Context, *, roll: str):
        """Roll some dice (via rolz.org API)"""
        if not self.re_roll.fullmatch(roll):
            raise UserInputError(ctx.bot.i18n.localize('roll_input_error', ctx=ctx))
        async with ctx.typing():
            data = await self.bot.get_api_json(f"https://rolz.org/api/?{roll}.json")
            result = str(data['result'])
            # check if site returned an error
            error_text = 'Error: '
            if result.startswith(error_text):
                result = result[len(error_text):]
                raise UserInputError(result)

            await ctx.send(result+data['details'])

    @commands.command()
    @commands.bot_has_permissions(attach_files=True)
    @commands.max_concurrency(5)
    async def download(self, ctx: commands.Context, *, url: str):
        """Downloads a YouTube video, or anything else supported by YouTube-DL."""
        async with ctx.typing():
            # find a format within discord upload limit
            try:
                meta = self.ytdl.extract_info(url, download=False)
            except DownloadError:
                raise UserInputError(ctx.bot.i18n.localize("ytdl_dl_error", ctx=ctx))
            if meta['playlist']:
                raise UserInputError(ctx.bot.i18n.localize("ytdl_playlist", ctx=ctx))

            # get max filesize
            guild: discord.Guild = ctx.guild
            guild_boosts = guild.premium_tier
            if guild_boosts == 2:
                filesize = 52428033  # 52428800 (50MiB) -767, not sure about this one
            elif guild_boosts == 3:
                filesize = 104856833  # 104857600 (100MiB) -767, not sure about this one
            else:
                filesize = 8388234  # 8389000 (8MiB) -767

            fmts = list(filter(lambda x: quality_filter(x, filesize, meta['extractor']), meta['formats']))
            if not fmts:
                raise NoOutputFormat(ctx.bot.i18n.localize("ytdl_error", ctx=ctx))
            fmt = max(fmts, key=lambda x: x['filesize'])

            # download file
            # we create a new YoutubeDL instance because there's no other way to set output file or format
            new_opts = self.ytdl_opts.copy()
            new_opts['format'] = fmt['format_id']
            out_path = os.path.join(CONSTANTS['temp_folder'], f"{uuid.uuid1()}-{meta['id']}.{fmt['ext']}")
            new_opts['outtmpl'] = out_path
            YoutubeDL(new_opts).download([url])

            # upload file
            out_args = (meta['title'], meta['uploader'], fmt['format'])
            out_str = ctx.bot.i18n.localize("ytdl_out", ctx=ctx).format(*map(esc_md, out_args))
            await ctx.reply(out_str, file=discord.File(out_path), mention_author=True)
            os.remove(out_path)


def setup(bot):
    bot.add_cog(Miscellaneous(bot))
